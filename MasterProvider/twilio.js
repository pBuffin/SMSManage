/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
var mysql = require('mysql');
var unirest = require('unirest');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'smsdatabase'
});
connection.connect();
var dateFormat = require('dateformat');
class twilio {

    send(phoneNumber, message, id) {
        unirest.post("https://AC36d22265969a2fb90873283f9f93183a:6aeb4926a93d8e4f95df214bbc599842@api.twilio.com/2010-04-01/Accounts/AC36d22265969a2fb90873283f9f93183a/Messages.json")
                .headers({'Accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'})
                .send({From: '+33644648260', To: phoneNumber, Body: message})
                .end(function (response) {
                    var status = response.body['status'];
                    var sid = response.body['sid'];
                    var ErrorCode = response.body['error_code'];
                    connection.query("select id,HomeServeStatusId from providerstatus where name = '" + status + "' AND idprovider = 1", function (err, rows, fields) {
                        if (!err) {
                            var statusid = rows[0].HomeServeStatusId;
                            var providerStatus = rows[0].id;
                            connection.query("UPDATE sms SET smsIdprovider = '" + sid + "',providerStatus = " + providerStatus + ",status=" + statusid +",ErrorCode = " + ErrorCode + ",idProvider = 1 WHERE id = '" + id + "'", function (err, rows, fields) {});
                        }
                    });
                });
    }

    checkStatus() {
        connection.query("select smsidprovider from sms where (status=2 OR status=3) AND idprovider = 1", function (err, rows, fields) {
            if (!err) {
                for (var i = 0; i < rows.length; i++) {
                    var sid = rows[i].smsidprovider;
                    unirest.post('https://AC36d22265969a2fb90873283f9f93183a:6aeb4926a93d8e4f95df214bbc599842@api.twilio.com/2010-04-01/Accounts/AC36d22265969a2fb90873283f9f93183a/Messages/' + sid + ".json")
                            .headers({'Accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'})
                            .send({"Body": ""})
                            .end(function (response) {
                                var dateSend = response.body['date_sent'];
                                var status = response.body['status'];
                                var errorCode = response.body['error_code']
                                var date = dateFormat(dateSend, "yyyy-mm-dd HH:mm:ss");
                                if (status != 409) {
                                    connection.query("Select id,HomeServeStatusId from ProviderStatus where name='" + status + "'  AND idprovider = 1", function (err, rows, fields) {
                                        if (!err) {
                                            var statusid = rows[0].HomeServeStatusId;
                                            var providerStatus = rows[0].id;
                                            connection.query("UPDATE sms SET status = " + statusid + ", ProviderStatus = " + providerStatus +",ErrorCode = " + errorCode +  ", SendDate='" + date + "' WHERE smsIdProvider = '" + sid + "';", function (err, rows, fields) {});
                                        }
                                    });
                                }
                            });
                }
            }
        });

    }
}

module.exports = twilio;