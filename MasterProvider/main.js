/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
const stompit = require('stompit');
var dateFormat = require('dateformat');

const twilio = require('./twilio.js');
const esendex = require('./esendex.js');

var providerStr = "twilio";

switch (providerStr) {
    case "twilio" :
        var provider = new twilio();
        break;

    case "esendex":
        var provider = new esendex();
        break;
}

stompit.connect({host: 'localhost', port: 61613}, (err, client) => {
    client.subscribe({destination: 'QueueSMS'}, (err, msg) => {
        msg.readString('UTF-8', (err, body) => {
            console.log(body);
            var data = JSON.parse(body);
            var phoneNumber = data['phoneNumber'];
            var message = data['message'];
            var id = data['id'];
            provider.send(phoneNumber, message, id);
        });
    });
});

function myFunc() {
    var now = new Date();
    console.log(dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT"));
    provider.checkStatus();
    setTimeout(myFunc, 30000);
}
setTimeout(myFunc, 10000);