CREATE TABLE `errorcodeprovider` (
  `errorcode` int(11) DEFAULT NULL,
  `errormessage` varchar(80) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `homeservestatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `messagetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `providerstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ProviderCode` varchar(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `HomeServeStatusId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_statut` (`HomeServeStatusId`),
  CONSTRAINT `FK_statut` FOREIGN KEY (`HomeServeStatusId`) REFERENCES `homeservestatus` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `sms` (
  `id` varchar(50) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `MessageType` int(11) DEFAULT NULL,
  `phoneNumber` varchar(13) DEFAULT NULL,
  `Message` varchar(250) DEFAULT NULL,
  `smsIdProvider` varchar(50) DEFAULT NULL,
  `creationdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SendDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modificationdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ProviderStatus` int(11) DEFAULT NULL,
  `ErrorCode` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_messagetype` (`MessageType`),
  KEY `FK_ProviderStatut` (`ProviderStatus`),
  KEY `FK_statutSMS` (`status`),
  CONSTRAINT `FK_messagetype` FOREIGN KEY (`MessageType`) REFERENCES `messagetype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `homeservestatus` (`name`) VALUES ('Delivered'),('Default'),('Waiting');