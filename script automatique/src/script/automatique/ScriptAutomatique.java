/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package script.automatique;

/**
 *
 * @author pbuffin
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.json.simple.JSONObject;

public class ScriptAutomatique {

    /**
     * @param args the command line arguments
     * @throws java.net.MalformedURLException
     */
    public static void main(String[] args) throws MalformedURLException, IOException {

        // Message parametre
        String phoneNumber = "0632586466";
        String message = "Bonjour";
        JSONObject data = new JSONObject();

        //Encoding JSON fromat
        data.put("phoneNumber", phoneNumber);
        data.put("message", message);

        String authStr = "username:password";
        String encoding = new sun.misc.BASE64Encoder().encode(authStr.getBytes());

        //Concatction webservice send endpoint
        URL url = new URL("http://localhost:8080/WebServiceSMS/webresources/SMS/Sends");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Authorization", "Basic " + encoding);
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        String input = data.toJSONString();
        OutputStream os = conn.getOutputStream();
        os.write(input.getBytes());
        os.flush();

        if (conn.getResponseCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
        }

        //parse response
        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
        String output;
        System.out.println("Output from Server .... \n");
        while ((output = br.readLine()) != null) {
            System.out.println(output);
        }
        conn.disconnect();
        //----------------------------------------------------------------------------------------------// 

//        String id = "136f2efd-8b37-4391-92bd-d2d44e1f7105";
//        String authStr = "username:password";
//        String encoding = new sun.misc.BASE64Encoder().encode(authStr.getBytes());
//        URL url = new URL("http://localhost:8080/WebServiceSMS/webresources/SMS/Checks/"+id);
//        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        conn.setRequestProperty("Authorization", "Basic "+encoding);
//        conn.setDoOutput(true);
//        conn.setRequestMethod("GET");
//        conn.setRequestProperty("Content-Type", "application/json");
//
//
//        if (conn.getResponseCode() != 200 && conn.getResponseCode() != 500) {
//            throw new RuntimeException("Failed : HTTP error code : "+ conn.getResponseCode());
//        }
//
//        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
//        String output = null;
//        System.out.println("Output from Server .... \n");
//        while ((output = br.readLine()) != null) {
//            System.out.println(output);
//        }
//        conn.disconnect();
        //-------------------------------------------------------------------------------------------//
    }
}
