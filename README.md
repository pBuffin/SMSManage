# Web Service SMS

## Pr�sentation

Ce web service permet d'envoyer des SMS professionnelles. Le web service r�ceptionne les informations n�cessaires � l�envoie du SMS, les sauvegardes en base de donn�es et les fait suivre � un serveur NodeJS par l�interm�diaire d� ActiveMQ. Le serveur NodeJS envoie les SMS aux API des diff�rents fournisseurs de services d�envoi de SMS (Twilio, Esendex,ect,..).

## Fonctionnement

### Script automatique

Il contacte simplement les 2 Endpoints du web service.

### WebServiceSMS

Il contient 2 Endpoints:<br>
*http://localhost:8080/WebServiceSMS/webresources/SMS/Sends*�: En POST, pour l�envoi de SMS.<br>
*http://localhost:8080/WebServiceSMS/webresources/SMS/Checks/{id}*: En GET pour conna�tre le statut de SMS en base de donn�es.<br>
Reporter vous au fichier SMSManage_swagger.yaml pour plus de pr�cision sur l� API, les informations consomm�es et celle produites.<br>
<br>
Lors de l�envoi, les  informations sont transmissent � un serveur NodeJS par l�interm�diaire d�un broker AcrtiveMQ.

### MasterProvider

Ce serveur en NodeJS r�ceptionne les information du SMS et les envoi au fournisseur SMS s�lectionner. Un syst�me de ��design Pattern�� permet d� implementer chaque API des diff�rents fournisseurs.

