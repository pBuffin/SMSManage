/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.homeserve.rest.WebServiceSMS;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.UUID;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 * REST Web Service
 *
 * @author pbuffin
 */
@Path("SMS")
public class SMSService {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of SMSService
     */
    public SMSService() {
    }

    /**
     * Retrieves representation of an instance of
     * org.homeserve.rest.WebServiceSMS.SMSService
     *
     * @param content
     * @return an instance of java.cd deslang.String
     * @throws java.net.MalformedURLException
     * @throws org.json.simple.parser.ParseException
     * @throws javax.jms.JMSException
     * @throws java.sql.SQLException Endpoint for send message with id,
     * phoneNumber and message in JSON body
     */
    @POST
    @Path("Sends")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject SendSMS(String content) throws MalformedURLException, IOException, ParseException, JMSException, SQLException {

        //parse content to JSON
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(content);

        //exctract data from JSON
        UUID uid = UUID.randomUUID();
        String id = uid.toString();
        jsonObject.put("id", id);
        String phoneNumber = (String) jsonObject.get("phoneNumber");
        String message = (String) jsonObject.get("message");

        final ConnectionFactory connFactory = new ActiveMQConnectionFactory();
        JSONObject jsonResponse = new JSONObject();
        String indicative = phoneNumber.substring(0, 2);

        //verified french cellular number and the lenght of the massage
        if ((indicative.equals("06") || indicative.equals("07")) && phoneNumber.length() == 10 && message.length() <= 160) {

            //save in DB
            DataBase DB = new DataBase();
            DB.SaveBase(id, message, phoneNumber);

            //Send message in queue
            final Connection conn = connFactory.createConnection();
            final Session sess = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
            final Destination dest = sess.createQueue("QueueSMS");
            final MessageProducer prod = sess.createProducer(dest);
            final Message msg = sess.createTextMessage(jsonObject.toJSONString());
            prod.send(msg);
            conn.close();
        } else {
            jsonResponse.put("default", "invalide data");
            return jsonResponse;
        }
        //return UUID in JSON
        jsonResponse.put("uuid", id);
        // fin de transmition provider SMS                
        return jsonResponse;
    }

    /**
     *
     * @param id
     * @return
     * @throws org.json.simple.parser.ParseException
     */
    @GET
    @Path("Checks/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public JSONObject CheckSMS(@PathParam("id") String id) throws ParseException {
       
        //check the sms's status in DB
        DataBase DB = new DataBase();
        String status = DB.CheckSMS(id);

        //return the status
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("status", status);

        return jsonResponse;
    }    
}
