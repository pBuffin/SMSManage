/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.homeserve.rest.WebServiceSMS;

import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.jms.JMSException;

/**
 *
 * @author pbuffin
 */
public class DataBase {

       String url = "jdbc:mysql://localhost:3306/smsdatabase";
        String utilisateur = "root";
        String motDePasse = "root";
        
    public DataBase() {
          try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
        }
          
    }

    public void SaveBase(String id, String Message, String phoneNumber) throws JMSException, SQLException {   

        try (Connection connexion = DriverManager.getConnection(url, utilisateur, motDePasse)) {
            /* Création de l'objet gérant les requêtes */
            Statement statement = (Statement) connexion.createStatement();
            statement.executeUpdate("INSERT INTO sms(id,status,MessageType,phoneNumber,Message) VALUES('" + id + "',3,1," + phoneNumber + "," + "'" + Message + "')");
            /* Ici, nous placerons nos requêtes vers la BDD */
            
        } catch (SQLException e) {
                System.out.println(e.getMessage());
        }
    }

    public String CheckSMS(String id) {

        String statut = "unknown id";

        try (Connection connexion = DriverManager.getConnection(url, utilisateur, motDePasse)) {

            /* Création de l'objet gérant les requêtes */
            Statement statement = (Statement) connexion.createStatement();
            /* Exécution d'une requête de lecture */
            ResultSet resultat = statement.executeQuery("select name from sms,homeservestatus where sms.id = '" + id + "' AND sms.status = homeservestatus.id");
            /* Ici, nous placerons nos requêtes vers la BDD */
           
            while (resultat.next()) {
                statut = resultat.getString("name");   
            }
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return statut;
    }
}
